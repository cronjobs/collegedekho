# importing the requests library
import requests

API_KEY = 'AIzaSyBx6Z-A8Ne9pMc57p3LcVnWuoE5yRVz5Og'
SAMPLE_URL = 'https://maps.googleapis.com/maps/api/geocode/json?address=Indian+School+of+Business+Management+Administration+(ISBM),+bangalore&key=AIzaSyBx6Z-A8Ne9pMc57p3LcVnWuoE5yRVz5Og';

def generate_url(name, city):
    address = name + " " + city
    address = address.replace(" ", "+")
    address = address.replace("&", ",")
    URL = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + address + '&key=' + API_KEY
    return URL

def get_lat_long_from_url(url):
    # sending get request and saving the response as response object
    #print(url);
    r = requests.get(url = url, params = {})

    # extracting data in json format
    data = r.json()
    #print(data);
    if len(data["results"]) == 0:
        return None
    # extracting latitude, longitude and formatted address
    # of the first matching location
    try:
        latitude = data['results'][0]['geometry']['location']['lat']
        longitude = data['results'][0]['geometry']['location']['lng']
        formatted_address = data['results'][0]['formatted_address']
    except:
        print("exception", url, data);
        return None

    # printing the output
    #print("Latitude:%s\nLongitude:%s\nFormatted Address:%s" %(latitude, longitude, formatted_address))

    return [latitude, longitude, formatted_address]


def correct_address(city, address):
    if city == "Bangalore" and "Bengaluru" in address:
        return True
    if city == "Bangalore" and "Bengaluru" in address:
        return True
    if city == "New Delhi" and "delhi" in address:
        return True
    if city == "Gurgaon" and "Gurugram" in address:
        return True
    return False


def get_lat_long_from_location(list):
    for college in list:
        city = college["city"]
        name = college["name"]
        url = generate_url(name, city)
        result = get_lat_long_from_url(url)
        if result:
            [latitude, longitude, formatted_address] = result
        if city not in formatted_address:
            print("wrong geo location", college, formatted_address);
            #break


def get_list():
    url = 'http://134.209.156.1:5000/api';
    r = requests.get(url = url, params = {})

    # extracting data in json format
    data = r.json()
    return data["colleges"]


data = [
    {
        "city": "Bangalore",
        "name": "Indian School of Business Management & Administration (ISBM)",
        "state": "Karnataka",
        "url": "https://www.collegedekho.com/colleges/isbm-bangalore"
        },
        {
        "city": "Ahmedabad",
        "name": "Indian School of Business Management & Administration (ISBM)",
        "state": "Gujarat", "url": "https://www.collegedekho.com/colleges/isbm-ahmedabad"
        }
    ]


get_lat_long_from_location(get_list())
