#!/usr/bin/python3
from flask import Flask, render_template, request, redirect, url_for
from flask_cors import CORS, cross_origin

from scraper.scraper.storage.flask_datalayer import get_colleges, get_cities, get_states, get_college, update_college, \
    db, get_college_within_radius
from scraper.scraper.storage.models import DATABASE_URI, College


def format_currency(amount, currency):
    if not amount:
        return None
    return '{} {:,.2f}'.format(currency.name, amount)


def pretty_none(value):
    if value is None:
        return ''
    return value


def yes_if_true(value, otherwise=''):
    if value is True:
        return 'Yes'
    return otherwise


def dl_item(key, value):
    if not value:
        return ''
    return '<div><dt>{}:</dt><dl>{}</dl></div>'.format(key, value)


app = Flask(__name__, template_folder='templates/')
cors = CORS(app)

app.config['CORS_HEADERS'] = 'Content-Type'
app.config['SQLALCHEMY_DATABASE_URI'] = DATABASE_URI
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

app.jinja_env.globals.update(format_currency=format_currency)
app.jinja_env.globals.update(pretty_none=pretty_none)
app.jinja_env.globals.update(yes_if_true=yes_if_true)
app.jinja_env.globals.update(dl_item=dl_item)

db.init_app(app)


@app.route('/')
def index():
    city_state = request.args.get('cityState', '')
    degree_type = request.args.get('degreeType', '')
    colleges, n_colleges = get_colleges(city_state=city_state, degree_type=degree_type)
    cities = get_cities()
    states = get_states()

    return render_template(
        'index.html', colleges=colleges, n_colleges=n_colleges,
        cityStates=[i[0] for i in list(cities)+list(states)],
        city_state=city_state, degree_type=degree_type,
        degree_types={i.degree.name for i in colleges})


@app.route('/edit/<int:college_id>', methods=['GET', 'POST'])
def edit(college_id):
    if request.method == 'POST':
        update_college(college_id, request.form)
        return redirect(url_for('index'))

    college = get_college(college_id)
    return render_template('college.html', college=college)


@cross_origin()
@app.route('/api')
def api():
    offset = int(request.args.get('offset', -1))
    limit = int(request.args.get('limit', -1))
    if offset < 0:
        offset = 0
    if limit <= 0 or limit > 100:
        limit = 100

    colleges, _ = get_colleges(offset, limit)
    return {"colleges": College.serialize_list(colleges)}


@app.route('/api/spatial_search')
def spatial_search():
    return {
        'colleges': [dict(i) for i in get_college_within_radius(
            float(request.args['latitude']),
            float(request.args['longitude']),
            float(request.args['radius']),
        )],
    }


if __name__ == '__main__':
    app.run(debug=True)
