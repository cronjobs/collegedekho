from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings

from scraper.scraper.spiders.collegedekho import CollegedekhoSpider

if __name__ == '__main__':
    get_project_settings()
    exit(0)
    process = CrawlerProcess(get_project_settings())

    process.crawl(CollegedekhoSpider)
    process.start()
