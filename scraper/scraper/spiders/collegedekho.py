import json
import logging
from urllib.parse import urlencode

import scrapy
from scrapy import Request

# from ..storage.models import  college_exists

API_KEY = 'AIzaSyBx6Z-A8Ne9pMc57p3LcVnWuoE5yRVz5Og'


def generate_url(name, city):
    url = 'https://maps.googleapis.com/maps/api/geocode/json?' +\
          urlencode({'address': name + ' ' + city, 'key': API_KEY})
    return url


def process_info(college):
    infos = college.css('ul.info > li::text').extract()

    rv = {}
    for info in infos:
        key, value = info.split(':')
        values = value.splitlines()
        if len(values) > 1:
            value = [i.strip() for i in values if i.strip()]
        else:
            value = value.strip()
        rv[key] = value

    return rv


class CollegedekhoSpider(scrapy.Spider):
    name = 'collegedekho'
    allowed_domains = ['collegedekho.com', 'maps.googleapis.com']
    start_urls = [
        'https://www.collegedekho.com/engineering/colleges-in-west-bengal/',
        'https://www.collegedekho.com/medical/colleges-in-india/'
    ]
    next_index_params = {
        'page': 1,
        'tags': 'medical',
        'load_more': 1,
        'geo_filter': False,
    }

    college_url_specified = False

    def __init__(self, college_url=None, lat_lng=False, **kwargs):
        super().__init__(**kwargs)
        if college_url:
            self.start_urls = [college_url]
            self.college_url_specified = True
        self.lat_lng = lat_lng

    def parse(self, response, **kwargs):
        self.next_index_params['csrfmiddlewaretoken'] = response.css('#csrf_token::attr(value)').get()

        if self.college_url_specified:
            yield from self.parse_college_page(response)
        else:
            yield from self.parse_index(response)

    def parse_index(self, response):
        selection = response.css('.col-md-12 .box')
        for college in selection:
            # college_id = college.css('[data-insti-id]::attr(data-insti-id)').get()
            # if not college_id or not college_exists(int(college_id)):
            #     yield response.follow(college.css('.title > a::attr(href)').get(), callback=self.parse_college_page)
            yield response.follow(college.css('.title > a::attr(href)').get(), callback=self.parse_college_page)

        if selection:
            yield self.next_index_page(response)

    def next_index_page(self, response):
        self.next_index_params['page'] += 1

        return response.follow(
            '/get_institutes_ajax/', method='POST', body=urlencode(self.next_index_params),
            callback=self.parse_index, headers={
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                'Referer': 'https://www.collegedekho.com/medical/colleges-in-india/',
                'Origin': 'https://www.collegedekho.com',
                'Host': 'www.collegedekho.com',
                'TE': 'Trailers',
                'X-Requested-With': 'XMLHttpRequest',
                'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) Gecko/20100101 Firefox/83.0',
            })

    def parse_college_page(self, response):
        # TODO: Scrape college type directly.
        meta = response.css('.infraList')
        info = dict(zip(meta.css('div.label::text').extract(), meta.css('td.data::text').extract()))
        college_id = response.css('meta[name=college_id]::attr(content)').get()
        city, state = response.css('span.location::text').get().strip().replace(')', '').split(' (')
        title = response.css('h1.tooltip::text').get().strip().split(', ', 1)[0]
        yield {
            'type': 'college',
            'data': {
                'id': college_id,
                'url': response.request.url,

                'title': title,

                'logo_url': response.css('div.collegeLogo > img::attr(src)').get(),
                'college_image_url': response.css('a.collegeImage > img::attr(data-gsll-src)').get(),
                'description': response.css('#a3 ::text').get().strip(),

                'city': city,
                'state': state,

                'type': info.get('College Type'),
                'degree': response.css('.bc-item a span::text').get().strip().split()[0],
                'established_in': info.get('Established In'),

                'rating': response.css('.star-ratings-sprite-rating::attr(style)').get(),
                'n_reviews': response.css('.rating-per span a::text').get(),

                'nirf': info.get('NIRF Ranking'),

                'fee_range': info.get('Fee Range'),
                # `fee_start` and `fee_end` will be handled by pipeline.
                # The pipeline will also delete `fee_range`.

                'foreign_exchange': info.get('Foreign Exchange Program'),
                'financial_assistance': info.get('Financial Assistance'),

                'n_courses': info.get('Courses Offered'),
                'total_intake': info.get('Total Intake'),

                'sf_ratio': info.get('Student Faculty Ratio'),
                'g_ratio': info.get('Gender Ratio'),

                'n_faculty': info.get('Total Faculty'),
            },
        }

        if self.lat_lng:
            yield Request(url=generate_url(title, city), callback=self.parse_lat_long, cb_kwargs={
                'college_id': college_id,
            })

    def parse_lat_long(self, response, college_id):
        data = json.loads(response.text)
        if data['status'] != 'OK':
            self.log(f'No location for college ID={college_id}', logging.WARN)
            return
        yield {
            'type': 'location',
            'data': {
                'college_id': college_id,
                'latitude': data['results'][0]['geometry']['location']['lat'],
                'longitude': data['results'][0]['geometry']['location']['lng'],
                'formatted_address': data['results'][0]['formatted_address'],
            }
        }
