import json

from sqlalchemy import or_, text

try:
    from scraper.scraper.storage.models import College, create_connection, create_tables
    from scraper.scraper.storage.flaskdb import db
    from scraper.scraper.storage import models
    engine = models.create_connection()
    models.create_tables(engine)

except ImportError:
    from scraper.storage.models import College
    from scraper.storage.flaskdb import db
    from scraper.storage import models


# TODO: Pagination
def get_colleges(offset=0, limit=-1, city_state=None, degree_type=None):
    colleges = db.session.query(College)

    if city_state:
        colleges = colleges.filter(or_(
            College.city == city_state,
            College.state == city_state,
        ))

    if degree_type:
        colleges = colleges.filter(College.degree == degree_type.upper())

    colleges.offset(offset)
    if limit > 0:
        colleges = colleges.limit(limit)

    colleges = colleges.all()
    n_colleges = len(colleges)

    return colleges, n_colleges


def get_college(college_id):
    college = db.session.query(College).get(college_id)

    return college


def college_exists(college_id):
    return db.session.query(College.id).filter_by(name='davidism').scalar() is not None


def update_college(college_id, values):
    college = db.session.query(College).filter(College.id == college_id)
    college.update(values)
    db.session.commit()

    return college


def get_cities():
    cities = db.session.query(College.city).distinct()

    return cities


def get_states():
    states = db.session.query(College.state).distinct()
    return states


def get_college_within_radius(latitude, longitude, radius):
    return db.engine.execute(text('''
    SELECT *,
    ( 6371 *
        ACOS(
            COS( RADIANS( latitude ) ) *
            COS( RADIANS( :latitude ) ) *
            COS( RADIANS( :longitude ) -
            RADIANS( longitude ) ) +
            SIN( RADIANS( latitude ) ) *
            SIN( RADIANS( :latitude) )
        )
    )
    AS distance FROM colleges HAVING distance <= :radius ORDER BY distance
    '''), latitude=latitude, longitude=longitude, radius=radius).fetchall()
