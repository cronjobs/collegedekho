import enum
from enum import auto

from sqlalchemy import Column, Integer, Text, String, create_engine, Enum, Float, Boolean, MetaData, inspect
from sqlalchemy.ext.declarative import declarative_base


DATABASE_URI = 'mysql://collegedekho:pass_coll_392@localhost/collegedekho'
# DATABASE_URI = 'mysql://root:root@localhost/collegedekho'


metadata = MetaData()
Base = declarative_base(metadata=metadata)


def create_tables(engine):
    Base.metadata.create_all(engine)


def create_connection():
    # TODO: Dynamic DSN?
    return create_engine(DATABASE_URI)


class CollegeTypeEnum(enum.Enum):
    PUBLIC = auto()
    GOVERNMENT = auto()
    STATE = auto()
    CENTRAL = auto()
    PRIVATE = auto()
    DEEMED_TO_BE_UNI = auto()
    DEEMED_PRIVATE = auto()


class CollegeFeeCurrencyEnum(enum.Enum):
    INR = auto()
    USD = auto()


class CollegeDegreeTypeEnum(enum.Enum):
    AGRICULTURE = auto()
    ARTS = auto()
    COMMERCE = auto()
    DENTAL = auto()
    DESIGN = auto()
    EDUCATION = auto()
    ENGINEERING = auto()
    HOTEL = auto()
    INFORMATION = auto()
    LAW = auto()
    MANAGEMENT = auto()
    MEDIA = auto()
    MEDICAL = auto()
    NURSING = auto()
    PARAMEDICAL = auto()
    PERFORMING = auto()
    PHARMACY = auto()
    PHYSICAL = auto()
    SCIENCE = auto()
    VOCATIONAL = auto()


class SerializerMixin:
    def serialize(self):
        d = {c: getattr(self, c) for c in inspect(self).attrs.keys()}
        for k, v in d.items():
            if isinstance(v, enum.Enum):
                d[k] = v.name

        return d

    @staticmethod
    def serialize_list(val):
        return [i.serialize() for i in val]


class College(Base, SerializerMixin):
    __tablename__ = 'colleges'

    # `id` is basically the value of meta[name=college_id]::attr(content)
    id = Column(Integer, primary_key=True, unique=True)
    url = Column(String(255))

    title = Column(String(255))

    logo_url = Column(String(255))
    college_image_url = Column(String(255))
    description = Column(String(3000))

    city = Column(String(32), index=True)
    state = Column(String(32), index=True)

    latitude = Column(Float)
    longitude = Column(Float)
    formatted_address = Column(String(512))

    type = Column("type", Enum(CollegeTypeEnum))
    degree = Column("degree", Enum(CollegeDegreeTypeEnum))
    established_in = Column(Integer)

    # TODO: Maybe add a review class?
    rating = Column(Float)
    n_reviews = Column(Integer)

    # NIRF Ranking
    nirf = Column(Integer)

    # The fee range.
    fee_start = Column(Float)
    fee_end = Column(Float)
    fee_currency = Column("fee_currency", Enum(CollegeFeeCurrencyEnum))

    foreign_exchange = Column(Boolean, default=False)
    financial_assistance = Column(Boolean, default=False)

    n_courses = Column(Integer)
    total_intake = Column(Integer)

    # Student faculty ratio.
    sf_ratio = Column(Float)

    # Gender ratio.
    g_ratio = Column(Float)

    n_faculty = Column(Integer)


# TODO: Since there are multiple rankings, implement a different
#  model for rankings.
